import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Question2 {
	public static void main(String args[]) {
		try {
			BufferedReader inp = new BufferedReader(new InputStreamReader(
					System.in));
			while (inp.ready()) {
				String input = inp.readLine();
				System.out.println(input);
			}
		} catch (IOException e) {
			System.out.println("Error");
		}
	}
}
