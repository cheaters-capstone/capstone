package services;

import java.io.File;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import domain.SourceCodeType;
import domain.Submission;

/**
 * <i>Note: This class is specific to the Cheaters.db SQLite database and is not
 * meant to be a general database access object.</br>An abstract class has been
 * provided should the database type need to be changed.</i></br></br>
 * Controller class for the Cheaters database. The database contains information
 * about the students submissions as well as the results of comparisons. The
 * database in use is SQLite 3.7.2.
 * 
 * @author Darryl Meyer
 * @see DatabaseAbstractClass
 * @since 17 August 2014
 */
public class DatabaseController extends DatabaseAbstractClass {

	/**
	 * This constructor takes in the location of where to save the database as a
	 * parameter. The database is created in the location specified and a
	 * connection to the database is opened. A submissions table is created in
	 * the database to store the contents of a submission.
	 * 
	 * @param rootPath
	 *            String of a folder of where to create the database object.
	 */
	public DatabaseController(String rootPath) {
		super(rootPath);
	}

	/**
	 * @see services.DatabaseAbstractClass#openDatabase()
	 */
	@Override
	public boolean openDatabase() {
		boolean status = false;

		try {
			// Load the SQLite JDBC driver
			Class.forName("org.sqlite.JDBC");

			// Connect to the database
			connection = DriverManager.getConnection("jdbc:sqlite:" + rootPath + database);

			status = true;
			System.out.println("Database opened successfully");

		} catch (Exception e) {
			System.err.println("Error opening database: " + e.getMessage());
		}

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#closeDatabase();
	 */
	@Override
	public boolean closeDatabase() {
		boolean status = false;

		try {
			connection.close();

			status = true;
			System.out.println("Database closed successfully");
		} catch (Exception e) {
			System.err.println("Error closing database: " + e.getMessage());
		}

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#isClosed();
	 */
	@Override
	public boolean isClosed() {
		boolean status = false;

		try {
			status = connection.isClosed();
		} catch (Exception e) {
			System.err.println("Error reading database closed state: " + e.getMessage());
		}

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#executeSQLUpdate(String);
	 */
	@Override
	public boolean executeSQLUpdate(String sql) {
		boolean queryStatus = false;
		Statement stmt = null;

		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}

		try {
			// Prepare SQL statement
			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			queryStatus = true;
			stmt.close();

		} catch (Exception e) {
			System.err.println("Error executing SQL query: " + e.getMessage() + " SQL: " + sql);
		}
		return queryStatus;
	}

	/**
	 * @see services.DatabaseAbstractClass#createSubmissionsTable()
	 */
	@Override
	public boolean createSubmissionsTable() {
		boolean status = false;

		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}
		try {
			// Create the Submissions table
			String sql = "CREATE TABLE IF NOT EXISTS SUBMISSIONS " + "(ID INTEGER PRIMARY KEY,"
					+ " USER TEXT NOT NULL, " + " ASSIGNMENT TEXT NOT NULL, " + " YEAR TEXT NOT NULL, "
					+ " TOKENS TEXT," + " FLAG TEXT NOT NULL," + " FILES TEXT NOT NULL)";

			Statement stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();

			status = true;
		} catch (Exception e) {
			System.out.println("Error creating submissions table: " + e + " " + e.getMessage());
		}

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#createUserTable(java.lang.String)
	 */
	@Override
	public boolean createUserTable(String user) {
		boolean status = false;
		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}

		Statement stmt = null;

		try {
			// Create the Submissions table
			String sql = "CREATE TABLE " + user + " (ID INTEGER PRIMARY KEY, " + " USER TEXT NOT NULL, "
					+ " ASSIGNMENT TEXT NOT NULL, " + " YEAR TEXT NOT NULL, " + " SIMILARITY TEXT NOT NULL)";

			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
			status = true;
		} catch (SQLException e) {
			System.err.println("Error creating table for: " + user + ". NOTE: This table may already exist. ");
		}

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#insertSubmission(domain.Submission)
	 */
	@Override
	public boolean insertSubmission(Submission submission) {
		boolean status = false;

		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}

		// Try create the table for users comparison results
		createUserTable(submission.getUser());

		// Create tokens string
		String tokens = "";
		if (!submission.getTokens().isEmpty()) {

			for (Integer t : submission.getTokens()) {
				tokens += t.toString() + ",";
			}

			// Remove trailing comma
			tokens = tokens.substring(0, tokens.length() - 1);
		} else {
			tokens = "null";
		}

		// Create file string
		String files = submission.getFile().getName();

		String sql = "INSERT OR REPLACE INTO SUBMISSIONS (ID, USER, ASSIGNMENT, YEAR, TOKENS, FLAG, FILES) "
				+ "VALUES ((SELECT ID FROM SUBMISSIONS WHERE USER = '"
				+ submission.getUser() + "' AND ASSIGNMENT = '"
				+ submission.getAssignment() + "' AND YEAR = '" + submission.getYear() + "'), '"
				+ submission.getUser() + "', '" + submission.getAssignment() + "', '"
				+ submission.getYear() + "', '" + tokens + "', '" + submission.getSourceCodeFlag() + "', '"
				+ files + "' );";
		status = executeSQLUpdate(sql);

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#insertComparison(domain.Submission,
	 * domain.Submission, double)
	 */
	@Override
	public boolean insertComparison(Submission submission1, Submission submission2, double similarity) {
		boolean status = false;

		String sql = "INSERT OR REPLACE INTO " + submission1.getUser()
				+ " (ID, USER,ASSIGNMENT,YEAR,SIMILARITY) " + "VALUES ( (SELECT ID FROM "
				+ submission1.getUser() + " WHERE USER = '"
				+ submission2.getUser() + "' AND ASSIGNMENT = '"
				+ submission2.getAssignment() + "' AND YEAR = '" + submission2.getYear() + "'), '"
				+ submission2.getUser() + "', '" + submission2.getAssignment() + "', '"
				+ submission2.getYear() + "', '" + similarity + "' );";

		status = executeSQLUpdate(sql);

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#startTransaction()
	 */
	@Override
	public boolean startTransaction() {
		boolean status = false;

		String sql = "BEGIN;";

		status = executeSQLUpdate(sql);

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#commitTransaction()
	 */
	@Override
	public boolean commitTransaction() {
		boolean status = false;

		String sql = "END TRANSACTION;";

		status = executeSQLUpdate(sql);

		return status;
	}

	/**
	 * @see services.DatabaseAbstractClass#selectSubmission(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public Submission selectSubmission(String user, String assignment, String year) {
		ResultSet rs = null;
		Statement stmt = null;
		Submission sub = null;

		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}

		try {
			// Select SQL statement
			stmt = connection.createStatement();
			System.out.println("SELECT * FROM SUBMISSIONS WHERE USER='" + user + "' AND ASSIGNMENT='"
					+ assignment + "' AND YEAR='" + year + "';");
			rs = stmt.executeQuery("SELECT * FROM SUBMISSIONS WHERE USER='" + user + "' AND ASSIGNMENT='"
					+ assignment + "' AND YEAR='" + year + "';");

			// Path to location of stored source code
			String path = baseSubmissionFolder + "/" + year + "/" + assignment + "/" + user + "/";

			// Populate the list of files
			String fileString = rs.getString("files");
			File submittedFile = new File(path + fileString);

			// Populate the integer list of tokens
			ArrayList<Integer> tokens = new ArrayList<Integer>();
			String tokenString = rs.getString("tokens");
			if (tokenString != null) {
				String[] tokenList = tokenString.split(",");
				for (int j = 0; j < tokenList.length; j++) {
					tokens.add(Integer.parseInt(tokenList[j]));
				}
			}

			// Else if to decide the source code flag
			String flag = rs.getString("flag");
			if (flag.equals("JAVA")) {
				sub = new Submission(user, assignment, year, SourceCodeType.JAVA, submittedFile);
				sub.setTokens(tokens);
			} else if (flag.equals("PYTHON")) {
				sub = new Submission(user, assignment, year, SourceCodeType.PYTHON, submittedFile);
				sub.setTokens(tokens);
			}

			stmt.close();
		} catch (Exception e) {
			System.err.println("Error selecting student from database: " + e + " " + e.getMessage());
		}

		return sub;
	}

	/**
	 * @see
	 * services.DatabaseAbstractClass#getComparisonResult(domain.Submission,
	 * domain.Submission)
	 */
	@Override
	public double getComparisonResult(Submission submission1, Submission submission2) {

		double result = 0;
		ResultSet rs = null;
		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}
		Statement stmt = null;

		try {
			// Select SQL statement
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM " + submission1.getUser()
					+ " WHERE USER='" + submission2.getUser() + "' AND ASSIGNMENT='"
					+ submission2.getAssignment() + "' AND YEAR='" + submission2.getYear() + "';");

			// Parse the result
			String similarity = rs.getString("similarity");
			result = Double.parseDouble(similarity);

			stmt.close();
		} catch (Exception e) {
			System.err.println("Error comparison result from database: " + e + " " + e.getMessage());
		}

		return result;
	}

	/**
	 * @see services.DatabaseAbstractClass#getSubmissionArray()
	 */
	@Override
	public ArrayList<Submission> getSubmissionArray() {
		ArrayList<Submission> submissions = new ArrayList<Submission>();

		ResultSet rs = null;
		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}
		Statement stmt = null;
		Submission sub = null;

		try {
			// Select SQL statement
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM SUBMISSIONS;");

			// Initialize variables
			String user = "";
			String year = "";
			String assignment = "";
			String path = "";
			ArrayList<Integer> tokens = null;
			String tokenString = "";
			String flag = "";

			// Loop through all results of the query
			while (rs.next()) {
				user = rs.getString("user");
				year = rs.getString("year");
				assignment = rs.getString("assignment");

				// Path to location of stored source code
				path = baseSubmissionFolder + "/" + year + "/" + assignment + "/" + user + "/";

				// Populate the list of files
				String fileString = rs.getString("files");
				File submittedFile = new File(path + fileString);

				// Populate the integer list of tokens
				tokens = new ArrayList<Integer>();
				tokenString = rs.getString("tokens");
				if (tokenString != null) {
					String[] tokenList = tokenString.split(",");
					for (int j = 0; j < tokenList.length; j++) {
						tokens.add(Integer.parseInt(tokenList[j]));
					}
				}

				// Else if to decide the source code flag
				flag = rs.getString("flag");
				if (flag.equals("JAVA")) {
					sub = new Submission(user, assignment, year, SourceCodeType.JAVA, submittedFile);
					sub.setTokens(tokens);
				} else if (flag.equals("PYTHON")) {
					sub = new Submission(user, assignment, year, SourceCodeType.PYTHON, submittedFile);
					sub.setTokens(tokens);
				}

				// Add submission to submission array
				submissions.add(sub);
			}

			stmt.close();
		} catch (Exception e) {
			System.err.println("Error getting submission array from database: " + e + " " + e.getMessage());
		}

		return submissions;
	}

	/**
	 * @see
	 * services.DatabaseAbstractClass#getSubmissionArray(domain.SourceCodeType)
	 */
	@Override
	public ArrayList<Submission> getSubmissionArray(SourceCodeType type) {
		ArrayList<Submission> submissions = getSubmissionArray();
		ArrayList<Submission> specificSubmissions = new ArrayList<Submission>();

		for (Submission s : submissions) {
			if (s.getSourceCodeFlag() == type) {
				specificSubmissions.add(s);
			}
		}

		return specificSubmissions;
	}

	/**
	 * Prints the working directory of the database to System.out.
	 */
	public void pwd() {
		// Check to see if the database connection is open
		if (isClosed()) {
			openDatabase();
		}

		try {
			System.out.println("Database path: " + connection.getMetaData().getURL());
		} catch (Exception e) {
			System.err.println("Could not print database working directory");
		}
	}
}
