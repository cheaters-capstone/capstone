package services;

import java.sql.Connection;
import java.util.ArrayList;

import domain.SourceCodeType;
import domain.Submission;

/**
 * This abstract class is provided should the type of database need to be
 * changed. For example a change from SQLite to MySQL. The methods in this
 * interface are required by other classes in the Cheaters Plagiarism Detection
 * system to perform database functions.
 * 
 * @author Darryl Meyer
 * @since 20 September 2014
 *
 */
public abstract class DatabaseAbstractClass {

	// Root path of database
	String rootPath = "";
	// Connection to database
	Connection connection;
	// Name of the database
	final String database = "Cheaters.db";
	// Path to saved Submission source code
	final String baseSubmissionFolder = "submissions";

	/**
	 * <strong>Classes that extend this abstract class only need to take the
	 * root path location as a parameter to the constructor and pass it
	 * super()</strong></br></br> This constructor takes in the location of where to
	 * save the database as a parameter. The database is created in the location
	 * specified and a connection to the database is opened. A submissions table
	 * is created in the database to store the contents of a submission.
	 * 
	 * @param rootPath
	 *            String of a folder of where to create the database object.
	 */
	public DatabaseAbstractClass(String rootPath) {
		this.rootPath = rootPath;
		openDatabase();
		createSubmissionsTable();
	}

	/**
	 * Open the connection to the database. This method also creates the
	 * database if it does not exist already.
	 * 
	 * @return Boolean - true if the database was opened successfully, false
	 *         otherwise.
	 */
	public abstract boolean openDatabase();

	/**
	 * Close the connection to the database.
	 * 
	 * @return Boolean - true if the database was closed successfully, false
	 *         otherwise.
	 */
	public abstract boolean closeDatabase();

	/**
	 * Checks to see if the connection to the database is closed. Returns a
	 * boolean to represent the status of the connection.
	 * 
	 * @return Boolean - true if the connection is closed, false otherwise.
	 */
	public abstract boolean isClosed();

	/**
	 * Creates a table in the database to store submissions. It will create the
	 * database file if it does not already exist. Returns true if the table was
	 * created successfully, false otherwise.
	 * 
	 * @return Returns true if the table was created successfully, false
	 *         otherwise.
	 */
	public abstract boolean createSubmissionsTable();

	/**
	 * Creates a table for a new user. The table is used to store the results of
	 * comparisons which involve the user's submissions. If the database has not
	 * been created yet a call to {@link createSubmissionTable} will be
	 * made.</br> <i>Note: This method may return false and display a System.err
	 * message that the table has not been created, in this case it is possible
	 * that the table being attempting to be made already exists.</i>
	 * 
	 * @param user
	 *            - The user id (student number) for whom the table will be
	 *            made.
	 * @return Returns true if the table for the user has been created, false
	 *         otherwise.
	 */
	public abstract boolean createUserTable(String user);

	/**
	 * Inserts a submission into the submission table. Should this method be
	 * called before the submission table is made, this method will attempt to
	 * make the submissions table.
	 * 
	 * @param submission
	 *            - Submission which is to be stored in the database's
	 *            submission table.
	 * @return True if the submission was successfully added, false otherwise.
	 */
	public abstract boolean insertSubmission(Submission submission);

	/**
	 * Inserts the result of a comparison between two submissions into the user
	 * table for whom submission1 belongs. The result to be stored in the result
	 * of the comparison of submission1 to submission2.
	 * 
	 * @param submission1
	 *            - Submission object belonging to the user for which the result
	 *            will be stored in their user table in the database.
	 * @param submission2
	 *            - Submission object who will be referenced in the result
	 *            stored in the user for submission1's table.
	 * @param similarity
	 *            - Type double result of the comparison of submission1 to
	 *            submission2.
	 * @return True if the insertion completed successfully, false otherwise.
	 */
	public abstract boolean insertComparison(Submission submission1, Submission submission2, double similarity);

	/**
	 * Begin a batch database transaction.
	 * 
	 * @return True if transaction has begun, false otherwise.
	 */
	public abstract boolean startTransaction();

	/**
	 * Commit a batch database transaction. Called after a batch of SQL update
	 * statements have been executed.
	 * 
	 * @return True if transaction has been committed, false otherwise.
	 */
	public abstract boolean commitTransaction();

	/**
	 * Queries the database and returns the Submission matching the parameters.
	 * 
	 * @param user
	 *            - User id of the submission
	 * @param assignment
	 *            - The name of the assignment for which the submission was
	 *            submitted
	 * @param year
	 *            - The year the submission was submitted.
	 * @return Submission object initialized to the contents that was saved to
	 *         the database, null if the submission does not exist.
	 */
	public abstract Submission selectSubmission(String user, String assignment, String year);

	/**
	 * Returns the similarity (result of a comparison) of submission1 compared
	 * to submission2.
	 * 
	 * @param submission1
	 *            - Submission object
	 * @param submission2
	 *            - Submission object
	 * @return double containing the similarity between the two submissions.
	 */
	public abstract double getComparisonResult(Submission submission1, Submission submission2);

	/**
	 * Returns an Array List of all the past submissions which were entered into
	 * the database. Returns all submissions regardless of source code type. To
	 * return only submissions of one source code type see {@link
	 * getSubmissionArray(SourceCodeType t)}.
	 * 
	 * @return ArrayList of type Submission - Array containing all that past
	 *         submissions.
	 */
	public abstract ArrayList<Submission> getSubmissionArray();

	/**
	 * Returns an Array List of submissions which were entered into the database
	 * containing only the submissions with source code matching the
	 * SourceCodeType parameter.
	 * 
	 * @param type
	 *            - SourceCodeType - type of source code for which submissions
	 *            to return.
	 * @return ArrayList of type Submission - Array containing only the
	 *         submissions with the specified source code.
	 */
	public abstract ArrayList<Submission> getSubmissionArray(SourceCodeType type);

	/**
	 * Generic method for performing a SQL update to the database that does not
	 * require data to be returned i.e. creating a new table, updating an
	 * existing entry or deleting an entry.
	 * 
	 * @param sql
	 *            String of query to execute.
	 * @return True if the update completed successfully, false otherwise.
	 */
	public abstract boolean executeSQLUpdate(String sql);

}