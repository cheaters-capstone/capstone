package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import domain.MatchDetails;
import domain.TokenItem;

/**
 * This class is responsible for finding matches between two lists of tokens.
 * 
 * Two Scores are calculated for the matches which represents the degree of similarity.
 * One score represents A:B, the other B:A.
 * 
 * Makes use of {@link #MatchDetails} and {@link #TokenItem}
 * 
 * @author Craig Feldman
 * @author Roscoe Jampies
 * 
 * Aug 2014
 *
 * @param <T> The type of objects stored in the lists that are going to be compared.
 */
public class PlagiarismDetector <T> {

	// Stores details about each match that has been found
	List<MatchDetails> matches;
	
	// The tokens that are going to be compared
	List<TokenItem<T>> list1;
	List<TokenItem<T>> list2;
	
	// The minimum required number of matches in a row
	int minMatchlen;
	
	// Score of list1 to list2
	double score1;
	// Score of list2 to list 1
	double score2;
	
	/**
	 * Constructs a new plagiarism detector object.
	 * @param l1 list of tokens to be compared to l2.
	 * @param l2 list of tokens to be compared to l1.
	 * @param minMatchlen the minimum length required for a match.
	 */
	public PlagiarismDetector(List<T> l1, List<T> l2, int minMatchlen) {
		list1 = new ArrayList<TokenItem<T>>();
		list2 = new ArrayList<TokenItem<T>>();

		for (T value : l1) 
			list1.add(new TokenItem<T>(value));
		
		for (T value : l2)
			list2.add(new TokenItem<T>(value));
		
		this.minMatchlen = minMatchlen;
		
		// stores the matches and its details for the calculations
		matches = new ArrayList<MatchDetails>();
		
		// -1 to indicate still needs to be computed
		score1 = -1;
		score2 = -1;
	}

	/**
	 * Compares two submissions using the greedy-string tiling algorithm.
	 * 
	 * Makes a call to {@link #filterMatches()} to select the most appropriate matches
	 * 
	 * @return ArrayList of type MatchDetails - matched tokens.
	 */
	 public List<MatchDetails> compareSubmissions() {

		// i -> position in first list
		// j -> position in the second list
		for (int i = 0; i < list1.size(); ++i) {
			for (int j = 0; j < list2.size(); ++j) {
				// length of the match that has been found
				int matchLength = 0;				
				
				// Counts how many matches in a row.
				while (list1.get(i + matchLength).getContent().equals(list2.get(j + matchLength).getContent())) {
					
					// if both are marked break				
					if (list2.get(j).marked() && list1.get(i).marked()) 
						break;										
					
					++matchLength;

					// if checked all of compareThis
					if ((matchLength + i) == list1.size()) 
						break;
						
					list1.get(matchLength + i).mark();
					
					// if checked all of toThis
					if ((matchLength + j) == list2.size()) 
						break;
						
					list2.get(matchLength + j).mark();
					
				}

				// if enough matches in a row
				if (matchLength >= minMatchlen) {
					
					MatchDetails previous;
					// adds the match to the list of matches
					if (!matches.isEmpty()) {
						previous = matches.get(matches.size() -1);
						
						// If the previous match has the same starting location, we must only add the better (longer) of the two matches
						if ((previous.getPositionInFirst() == i) && previous.getLength() < matchLength) {
								matches.remove(matches.size() - 1);
								matches.add(new MatchDetails(i, j, matchLength));
						}

						else if ((previous.getPositionInFirst() != i))
							matches.add(new MatchDetails(i, j, matchLength));
					}
					else
						matches.add(new MatchDetails(i, j, matchLength));					
				}
			}
		}		

		filterMatches();		
		return matches;
	}

	/**
	 * If two matches have the same starting location, selects the longer match and removes the shorter match.
	 * If two matches overlap, removes the shorter match.
	 */
	private void filterMatches() {
		
		//determines how far ahead we should look for overlapped matches in the match list.
		final int LOOK_AHEAD = 10;

		// filter to extract the best  overlapping matches		
		// Marks a match as invalid to be removed later.
		for (int i = 0; i < matches.size(); ++i) {
			MatchDetails current = matches.get(i);

			for (int j = i + 1; (j < i + LOOK_AHEAD) && (j < matches.size()); ++j) {
				MatchDetails next = matches.get(j);	
				
				int endIndex = current.getPositionInFirst() + current.getLength();
				
				//if overlap
				if ((next.getPositionInFirst() >= current.getPositionInFirst()) && (next.getPositionInFirst() <= endIndex)) {
					if (current.getLength() < next.getLength()) 
						current.invalidate();
					else 
						next.invalidate();							
				}
			}
		}
		
		
		// ------------------------------------------------------------------------
		// Repeat the above, this time for the matches in list2
		
		//sort by position in second to effectively so that the checks will work correctly
		Collections.sort(matches, new Comparator<MatchDetails>() {
		    public int compare(MatchDetails m1, MatchDetails m2) {
		        return Integer.compare(m1.getPositionInSecond(), m2.getPositionInSecond());
		    }
		});
				
		// Select best match if same starting location
		// Works from back of list to minimise performance effect of deletion
		for (int i = matches.size() - 1; i > 0 ; --i) {
			MatchDetails current = matches.get(i);
			MatchDetails next = matches.get(i - 1);

			if (current.getPositionInSecond() == next.getPositionInSecond()) {
				//remove current
				if (current.getLength() <= next.getLength()) 
					matches.remove(i);				
				//remove next
				else 
					matches.remove(--i);
			}
		}
					
		// filter to extract the best overlapping matches based on position in second
		for (int i = 0; i < matches.size(); ++i) {
			MatchDetails current = matches.get(i);

			for (int j = i + 1; (j < i + LOOK_AHEAD) && (j < matches.size()); ++j){
				MatchDetails next = matches.get(j);	
				
				int endIndex = current.getPositionInSecond() + current.getLength();
				
				//if overlap
				if ((next.getPositionInSecond() >= current.getPositionInSecond()) && (next.getPositionInSecond() <= endIndex)) {
					if (current.getLength() < next.getLength()) 
						current.invalidate();
					else 
						next.invalidate();						
				}
			}
		}
		
		// Remove matches marked for deletion
		// Start from back of list for performance
		ListIterator<MatchDetails>iter = matches.listIterator(matches.size());
		while (iter.hasPrevious()) {
			if (iter.previous().getValid() == false ) 
				iter.remove();
		}
	}
	

	/** Computes the similarity score for list1 to list2 and list2 to list1. */
	private void computeScore() {

		// double to prevent integer division
		double totalMatchedLength = 0;

		for (MatchDetails match : matches) 
			totalMatchedLength += match.getLength();
	
		score1 =  totalMatchedLength / list1.size();
		score2 = totalMatchedLength / list2.size();		
	}
	
	
	// ================================================================================
	// Accessors
	// ================================================================================	
	
	/** @return a list containing {@link #MatchDetails} objects	*/
	public List<MatchDetails> getMatches() {
		return matches;
	}

	/**
	 * Computes comparison score if not already computed.
	 * @return the score of submission 1 relative to submission 2
	 */
	public double getFirstScore() {
		if (score1 == -1)
			computeScore();
		return score1;
	}

	/**
	 * Computes comparison score if not already computed
	 * @return the score of submission 2 relative to submission 1
	 */
	public double getSecondScore() {
		if (score2 == -1)
			computeScore();
		return score2;
	}
	
}
