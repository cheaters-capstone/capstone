package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class handles the user logging out. The login session variable will be
 * set to false.
 * 
 * @author Darryl Meyer
 * @since 2 September 2014
 */
@WebServlet("/Logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public HttpSession session = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Logout() {
		super();
	}

	/**
	 * The login session variable is set to false. This variable is used to
	 * check whether a user has logged in.
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		session = request.getSession();
		session.setAttribute("login", "false");
		request.setAttribute("output", "You are now logged out.");
		getServletContext().getRequestDispatcher("/Output.jsp").forward(request, response);
	}

}
