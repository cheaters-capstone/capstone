package servlets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.PlagiarismDetector;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import services.DatabaseController;
import domain.GeneralReport;
import domain.MatchDetails;
import domain.PairReport;
import domain.SourceCodeType;
import domain.Submission;

/**
 * Servlet implementation class UploadBatch. Main class to handle all aspects of
 * the plagiarism detection system.
 * 
 * @author Darryl Meyer
 * @author Craig Feldman
 * @since 2 September 2014
 */
@SuppressWarnings("serial")
@WebServlet("/UploadBatch")
public class Upload extends HttpServlet {

	// Default upload path to store submitted files
	private String fileUploadPath = "FileUpload";
	// Root storage path / location where the server files are
	private String rootPath = "";
	// Root of the extracted zip
	private String rootZipFolder = "";
	// Maximum upload file size (default 40MB)
	private static final int mazFileSize = 1024 * 1024 * 40;
	// Calendar used to get current date and time
	private Calendar calendar;
	// Used to format the calendar to get the year
	private DateFormat dateYearFormat;
	// Used when saving to storage, saves to folder reports/year/date/
	private DateFormat dateFormat;
	// Date and time that the report was submitted
	// Will be formatted to the dateFormat specification
	private String date;
	// Database object
	private DatabaseController database;
	// Array list of all submissions
	private ArrayList<Submission> submissions;
	// For formatting the percentage to the nearest 2 decimal places
	private DecimalFormat decimal = new DecimalFormat("#0.00");
	// Boolean true if the batch input form was used
	private boolean batchSubmission = false;
	// Type of source code submitted
	private SourceCodeType sourceType;

	// The following variables are used as performance counters
	long timeStart = 0;
	long timeEnd = 0;
	long timeWriteStart = 0;
	long timeWriteTotal = 0;
	long timeComparisonStart = 0;
	long timeComparisonTotal = 0;
	long timeDatabaseStart = 0;
	long timeDatabaseTotal = 0;
	int comparisonCount = 0;

	/**
	 * Called when the Servlet begins it's lifecycle. Initializes the database,
	 * sets the root path, initializes the submissions array and sets the
	 * dateFormat.
	 * 
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		submissions = new ArrayList<Submission>();
		rootPath = getServletContext().getRealPath("") + File.separator;
		database = new DatabaseController(rootPath);
		dateFormat = new SimpleDateFormat("dd-MM-YYYY_HH-mm");
	}

	/**
	 * Called when the Servlet reaches the end of it's lifecycle. Closes the
	 * connection to the database.
	 * 
	 * @see javax.servlet.GenericServlet#destroy()
	 */
	@Override
	public void destroy() {
		database.closeDatabase();
	}

	/**
	 * Handle posts to this Servlets from the Upload.jsp. This method handles
	 * both the submission of a single file as well as the submission of a batch
	 * file.
	 * 
	 * @see HttpServlet
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		// Current system time to count total program run time
		timeStart = System.currentTimeMillis();

		// Used when uploading file from form input
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// Set temporary file storage location
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		// Servlet to handle upload to temporary location
		ServletFileUpload upload = new ServletFileUpload(factory);
		// Set maximum file upload size
		upload.setFileSizeMax(mazFileSize);

		// Path to store temporary uploaded files
		String uploadPath = rootPath + fileUploadPath;

		// Create upload directory if it does not exist
		File uploadDir = new File(uploadPath);
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		}

		try {
			// Parse HttpServletRequest to get input data from the input forms
			List<FileItem> fileItems = upload.parseRequest(request);

			if (fileItems != null && fileItems.size() > 0) {
				String assignment = "";
				String user = "";
				String filename = "";
				String type = "";
				String filePath = uploadPath + File.separator + filename;
				File uploadedFile = null;

				for (FileItem item : fileItems) {

					// processes only fields that are not form fields
					if (!item.isFormField()) {

						if (item.getFieldName().equals("uploadBatchFile") && item != null) {
							// The batch input form was used

							batchSubmission = true;
							filename = new File(item.getName()).getName();
							filePath = uploadPath + File.separator + filename;
							uploadedFile = new File(filePath);

							// Save the file to disk
							item.write(uploadedFile);
							System.out.println("\nBatch file submission detected");
						} else if (item.getFieldName().equals("uploadSingleFile") && item != null) {
							// The single file input form was used

							batchSubmission = false;
							filename = new File(item.getName()).getName();
							filePath = uploadPath + File.separator + filename;
							File file = new File(filePath);

							// Save the file to disk
							item.write(file);
							uploadedFile = file;
							System.out.println("\nSingle file submission detected");
						}
					}

					// User input field
					if (item.getFieldName().equals("user")) {
						user = item.getString();
					}

					// Assignment input field
					if (item.getFieldName().equals("assignment")) {
						assignment = item.getString();
					}

					// Decide the type of source code submitted
					if (item.getFieldName().equals("sourceType")) {
						type = item.getString();

						if (type.equals("java")) {
							sourceType = SourceCodeType.JAVA;
						} else if (type.equals("python")) {
							sourceType = SourceCodeType.PYTHON;
						}
					}
				}

				if (batchSubmission) {
					// The case of a batch submission

					// Clear submissions list
					submissions = new ArrayList<Submission>();

					if (uploadedFile != null) {
						submitBatchFile(uploadedFile, request, response);
						uploadedFile = null;
					}
				} else {
					// The case of a single file submission

					if (uploadedFile != null && !user.equals("") && !assignment.equals("")) {
						submitSingleFile(user, assignment, uploadedFile, request, response);
					}
				}

			}
		} catch (Exception e) {
			// Show output in the case of an error
			e.printStackTrace();
			request.setAttribute("output", "Submit unsuccessful: " + e + " " + e.getMessage());
			getServletContext().getRequestDispatcher("/Output.jsp").forward(request, response);
		}
	}

	/**
	 * Compares the files in the zip archive submitted using the batch
	 * submission input form. Only files of the selected file type are compared,
	 * otherwise if the file type does not match the selected source code type
	 * to compare, they will be listed as unsuccessful submissions. The files
	 * are tokenized then their tokens are compared using the Plagiarism
	 * Detector class. A Pair Report of the comparisions of the files is created
	 * if the maximum of the cheating similarity between the two files is
	 * greater than the predefined cutoff value. A General Report listing the
	 * Pair Reports is than created.
	 * 
	 * @see PlagiarismDetector
	 * @see PairReport
	 * @see GeneralReport
	 * @return String of the location of the General Report formatted as an HTML
	 *         hyperlink.
	 */
	private String compareBatchSubmissions() {
		ArrayList<PairReport> reports = new ArrayList<PairReport>();
		// Location of the General Report formatted as an HTML hyper link
		String reportLocation = "";

		Submission submission1 = null;
		Submission submission2 = null;
		List<Integer> list1 = null;
		List<Integer> list2 = null;

		// Minimum similarity for a report to be generated
		final double CUTOFF = 0.2;

		System.out.println("Submissions size: " + submissions.size());

		// Set date so that all reports in this batch have the same date
		calendar = Calendar.getInstance();
		date = dateFormat.format(calendar.getTime());

		// Start a database transaction
		database.startTransaction();
		// Loop through all submissions to compare each against each other
		if (submissions.size() >= 2) {
			for (int i = 0; i < submissions.size(); i++) {
				for (int j = i + 1; j < submissions.size(); j++) {
					submission1 = submissions.get(i);
					submission2 = submissions.get(j);
					list1 = submission1.getTokens();
					list2 = submission2.getTokens();

					// Create a new plagiarism detector object to compare the
					// token lists
					PlagiarismDetector<Integer> cheater = new PlagiarismDetector<Integer>(list1, list2, 3);

					timeComparisonStart = System.currentTimeMillis();
					// Compare submissions
					cheater.compareSubmissions();
					timeComparisonTotal += (System.currentTimeMillis() - timeComparisonStart);
					comparisonCount++;

					double cheatingPercentage1 = cheater.getFirstScore();
					double cheatingPercentage2 = cheater.getSecondScore();

					// Insert the results of the comparison into the database
					timeDatabaseStart = System.currentTimeMillis();
					database.insertComparison(submission1, submission2, cheatingPercentage1);
					database.insertComparison(submission2, submission1, cheatingPercentage2);
					timeDatabaseTotal += (System.currentTimeMillis() - timeDatabaseStart);

					if (Math.max(cheatingPercentage1, cheatingPercentage2) > CUTOFF) {
						// Generate Report
						PairReport report1 = new PairReport(submission1, cheatingPercentage1, submission2,
								cheatingPercentage2, date);

						// Generate left and right frames
						ArrayList<MatchDetails> matches = (ArrayList<MatchDetails>) cheater.getMatches();

						timeWriteStart = System.currentTimeMillis();
						// Generate HTML file containing submission content
						report1.writeReport(rootPath);
						report1.generateHTML(matches, rootPath);
						reports.add(report1);
						timeWriteTotal = timeWriteTotal + (System.currentTimeMillis() - timeWriteStart);
					}
				}
			}

			// Commit the database transaction
			database.commitTransaction();

			// Create the general report to list the individual pair reports
			GeneralReport generalReport = new GeneralReport(reports, date);
			timeWriteStart = System.currentTimeMillis();
			generalReport.writeReport(rootPath);
			timeWriteTotal = timeWriteTotal + (System.currentTimeMillis() - timeWriteStart);
			reportLocation = "<a href=\"" + generalReport.getLocalWritePath().replace(" ", "%20")
					+ File.separator + generalReport.getFilename() + "\">" + generalReport.getFilename()
					+ "</a></br>";

			// Clears submissions from array list
			submissions.clear();
			// Reset the root folder name
			rootZipFolder = "";
			// Clear file upload directory
			clearFileUploadDirectory();
		}
		return reportLocation;
	}

	/**
	 * Compares the file submitted by the user against all other files in the
	 * database. Only files of the selected file type are compared. The file is
	 * tokenized and its tokens are compared using the Plagiarism Detector
	 * class. A String of the value of the highest similarity between the
	 * submitted file and the other files is return.
	 * 
	 * @param type
	 *            The type of source code to compare against
	 * @return String of the highest similarity of the submitted file when
	 *         compared against all previously submitted files.
	 */
	private String compareSingleSubmission(SourceCodeType type) {
		// Similarity between the two compared files
		double similarity = 0;
		// Highest similarity between the compared files
		double highestSimilarity = 0;

		Submission submission1 = null;
		Submission submission2 = null;
		List<Integer> list1 = null;
		List<Integer> list2 = null;

		// Get all previous submissions from the database that are of the same
		// source code type
		ArrayList<Submission> submissions = database.getSubmissionArray(type);

		System.out.println("Submissions array size: " + submissions.size());
		if (submissions.size() >= 2) {
			// Start a database transaction
			database.startTransaction();
			for (int i = 0; i < submissions.size(); i++) {
				for (int j = i + 1; j < submissions.size(); j++) {
					submission1 = submissions.get(i);
					submission2 = submissions.get(j);
					list1 = submission1.getTokens();
					list2 = submission2.getTokens();

					// Create a new plagiarism detector object to compare the
					// token lists
					PlagiarismDetector<Integer> cheater = new PlagiarismDetector<Integer>(list1, list2, 3);
					timeComparisonStart = System.currentTimeMillis();
					cheater.compareSubmissions();
					timeComparisonTotal += (System.currentTimeMillis() - timeComparisonStart);
					comparisonCount++;

					double cheatingPercentage1 = cheater.getFirstScore();
					double cheatingPercentage2 = cheater.getSecondScore();

					// Find the highest similarity
					similarity = Math.max(cheatingPercentage1, cheatingPercentage2);
					if (similarity > highestSimilarity) {
						highestSimilarity = similarity;
					}

					// Save comparison results to the database
					timeDatabaseStart = System.currentTimeMillis();
					database.insertComparison(submission1, submission2, cheatingPercentage1);
					database.insertComparison(submission2, submission1, cheatingPercentage2);
					timeDatabaseTotal += (System.currentTimeMillis() - timeDatabaseStart);

				}
			}
			// Commit the database transaction
			database.commitTransaction();
		}
		return decimal.format(highestSimilarity * 100);
	}

	/**
	 * Submits a single file from the batch submission to the database. Creates
	 * a new Submission object and passes that to the database to be inserted
	 * into the submissions table.
	 * 
	 * @param user
	 *            The student ID of the student who submitted.
	 * @param assignment
	 *            The assignment for which the submission was submitted.
	 * @param file
	 *            File the student submitted.
	 * @return True if the submission was submitted successfully to that
	 *         database, false otherwise.
	 */
	private boolean submitSingleFileFromBatch(String user, String assignment, File file) {
		boolean submitStatus = false;
		user = user.toLowerCase();

		// Get the current year
		dateYearFormat = new SimpleDateFormat("yyyy");
		calendar = Calendar.getInstance();
		String year = dateYearFormat.format(calendar.getTime());

		// Determine source code type
		String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1,
				file.getName().length());
		extension = extension.toLowerCase();

		Submission submission = null;

		// Determine the SourceCodeType
		if (sourceType.equals(SourceCodeType.JAVA) && extension.equals("java")) {
			submission = new Submission(user, assignment, year, SourceCodeType.JAVA, file);
		} else if (sourceType.equals(SourceCodeType.PYTHON) && extension.equals("py")) {
			submission = new Submission(user, assignment, year, SourceCodeType.PYTHON, file);
		}

		if (submission != null) {
			// Save submission to database
			boolean status = database.createSubmissionsTable();
			status = database.insertSubmission(submission);

			// Save the source code to storage
			submission.writeSourceCode(rootPath);

			// Add the submission the the submission array list
			submissions.add(submission);

			submitStatus = status;
		}

		return submitStatus;
	}

	/**
	 * Submits a single file submission to the database. Will call
	 * {@link #compareSingleSubmission(SourceCodeType)} to compare the
	 * submission with all previous submissions. Displays as output to the user
	 * using the Output.jsp the highest similarity between the submitted file
	 * and the other files already in the database.
	 * 
	 * @param user
	 *            The student ID of the student who submitted.
	 * @param assignment
	 *            The assignment for which the submission was submitted.
	 * @param file
	 *            File the student submitted.
	 * @param request
	 *            HttpServletRequest associated with this servlet.
	 * @param response
	 *            HttpServletResponse associated with this servlet.
	 */
	private void submitSingleFile(String user, String assignment, File file, HttpServletRequest request,
			HttpServletResponse response) {
		user = user.toLowerCase();

		// Get the current year
		dateYearFormat = new SimpleDateFormat("yyyy");
		calendar = Calendar.getInstance();
		String year = dateYearFormat.format(calendar.getTime());

		// Get the extension of the file submitted.
		String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1,
				file.getName().length());
		extension = extension.toLowerCase();

		Submission submission = null;

		// Determine the SourceCodeType
		if (sourceType == SourceCodeType.JAVA && extension.equals("java")) {
			submission = new Submission(user, assignment, year, SourceCodeType.JAVA, file);
		} else if (sourceType == SourceCodeType.PYTHON && extension.equals("py")) {
			submission = new Submission(user, assignment, year, SourceCodeType.PYTHON, file);
		}

		if (submission != null) {
			// Save submission to database
			boolean status = database.insertSubmission(submission);

			// Save the source code to storage
			submission.writeSourceCode(rootPath);

			// If the submission was inserted successfully into the database.
			if (status) {
				String percentage = compareSingleSubmission(submission.getSourceCodeFlag());

				// Set output for Output.jsp
				request.setAttribute("output", "Your assignment has been submitted successfully.</br>"
						+ "The highest similarity between your submission and "
						+ "previous submissions by other students is: " + percentage + "%");

			} else {
				// Set output for Output.jsp
				request.setAttribute("output", "Your file could not be submitted to the database.");
			}
		} else {

			// Set output for Output.jsp
			request.setAttribute("output", "Either the source code type of your submitted file is not supported by this system</br>"
					+ "or the source code type you selected is different from the source code you submitted.");
		}

		try {
			// Forward output to the Output.jsp
			getServletContext().getRequestDispatcher("/Output.jsp").forward(request, response);

			// Current system time to end count of total program run time
			timeEnd = System.currentTimeMillis();

			// Used to display performance counters
			System.out.println("Execution time (ms): " + (timeEnd - timeStart));
			System.out.println("Comparison time (ms): " + (timeComparisonTotal));
			System.out.println("Database time (ms): " + (timeDatabaseTotal));
			System.out.println("Comparisions: " + comparisonCount);

			timeWriteTotal = 0;
			comparisonCount = 0;
			timeComparisonTotal = 0;
			timeDatabaseTotal = 0;
		} catch (ServletException e) {
			System.out.println("Servlet Exception: " + e);
		} catch (IOException e) {
			System.out.println("IO Exception: " + e);
		}

	}

	/**
	 * Submits a batch file submission to the database. Will call
	 * {@link #compareBatchSubmissions()} to compare all the submissions in the
	 * batch submission matching the source code type selected. Displays as
	 * output to the user reports detailing the comparison between the files.
	 * 
	 * @param uploadedZippedFile
	 *            The zipped, batch file of submissions.
	 * @param request
	 *            HttpServletRequest associated with this servlet.
	 * @param response
	 *            HttpServletResponse associated with this servlet.
	 */
	private void submitBatchFile(File uploadedZippedFile, HttpServletRequest request,
			HttpServletResponse response) {

		// Unzip the submitted file
		boolean zipStatus = unZipFile(uploadedZippedFile);
		// Files matching the source code type selected and submitted
		// successfully to the database.
		String successfulSubmissions = "";
		// Files that did not match the source code type selected.
		String unsuccessfulSubmissions = "";

		// zipStatus true if the file was unzipped successfully
		if (zipStatus) {
			// The name of the assignment is the name of the root folder in the
			// zipped archive
			String assignment = rootZipFolder;
			// Path to the location of the files running on the server
			String rootPath = getServletContext().getRealPath("") + File.separator + fileUploadPath
					+ File.separator + rootZipFolder;

			File rootFolder = new File(rootPath);
			rootPath = getServletContext().getRealPath("") + File.separator + fileUploadPath + File.separator
					+ rootZipFolder + File.separator;

			ArrayList<String> users = new ArrayList<String>();

			// Get a list of all the names of the folders that correspond to
			// users
			for (File fileEntry : rootFolder.listFiles()) {
				if (fileEntry.isDirectory()) {
					users.add(fileEntry.getName());
				}
			}

			if (!users.isEmpty()) {

				File submission = null;
				// For each user folder submit the files contained in it
				for (String user : users) {

					File userSubmission = new File(rootPath + user);
					File[] userSubmittedFiles = userSubmission.listFiles();

					// If the user submitted more than one file for the
					// assignment it is merged into one file.
					if (userSubmittedFiles.length > 1) {
						// Case of more than one file submitted

						// Create a string of the contents of the files to be
						// merged
						String mergedFileContents = "";

						for (File fileEntry : userSubmittedFiles) {
							if (!fileEntry.isDirectory()) {

								submission = new File(rootPath + user + File.separator + fileEntry.getName());

								String extension = submission.getName().substring(
										submission.getName().lastIndexOf('.') + 1,
										submission.getName().length());

								// Only merge files of the same source code type
								// and if they match the selected source code
								// type
								if (extension.equalsIgnoreCase("java")
										&& sourceType.equals(SourceCodeType.JAVA)) {
									try {
										mergedFileContents += new String(Files.readAllBytes(submission
												.toPath()));
									} catch (IOException e) {
										e.printStackTrace();
										System.err.println("Error merging file: " + submission.getName());
									}
								} else if (extension.equalsIgnoreCase("py")
										&& sourceType.equals(SourceCodeType.PYTHON)) {
									try {
										mergedFileContents += new String(Files.readAllBytes(submission
												.toPath()));
									} catch (IOException e) {
										e.printStackTrace();
										System.err.println("Error merging file: " + submission.getName());
									}
								}
							}
						}

						// The file extension depends on the source type
						// submitted
						if (sourceType.equals(SourceCodeType.JAVA)) {
							submission = new File(rootPath + user + File.separator
									+ "CombinedSubmission.java");

						} else if (sourceType.equals(SourceCodeType.PYTHON)) {
							submission = new File(rootPath + user + File.separator + "CombinedSubmission.py");
						}

						// Write the contents of the merged files to a single file
						if (!mergedFileContents.equals("")) {
							BufferedWriter bw;
							try {
								bw = new BufferedWriter(new FileWriter(submission.getAbsoluteFile()));
								bw.write(mergedFileContents);
								bw.close();
							} catch (IOException e) {
								System.err.println("Error writing merged file for user: " + user);
							}

							boolean submitted = submitSingleFileFromBatch(user, assignment, submission);
							if (submitted) {
								successfulSubmissions += user + " " + assignment + " " + submission.getName()
										+ " </br>";
							} else {
								unsuccessfulSubmissions += user + " " + assignment + " "
										+ submission.getName() + " </br>";
							}
						}

					} else if (userSubmittedFiles.length == 1) {
						// Case of a single file submitted

						File fileEntry = userSubmittedFiles[0];
						if (!fileEntry.isDirectory()) {
							submission = new File(rootPath + user + File.separator + fileEntry.getName());

							// Determine file extension
							String extension = submission.getName().substring(
									submission.getName().lastIndexOf('.') + 1, submission.getName().length());

							// If the extension is supported by the system
							if (extension.equalsIgnoreCase("java") || extension.equalsIgnoreCase("py")) {

								boolean submitted = submitSingleFileFromBatch(user, assignment, submission);
								if (submitted) {
									// Case of a successful submission
									
									successfulSubmissions += user + " " + assignment + " "
											+ submission.getName() + " </br>";
								} else {
									// Case of a unsuccessful submission
									
									unsuccessfulSubmissions += user + " " + assignment + " "
											+ submission.getName() + " </br>";
								}
							}
						} else {
							System.err.println("DIR: " + fileEntry.getName());
						}
					}
				}
			}
		}

		try {
			// Generate output to list the files submitted
			StringBuilder outputString = new StringBuilder();
			outputString.append("</br><strong>Successful file submissions:</strong></br>");
			outputString.append(successfulSubmissions);

			// List all unsuccessful submissions
			// An unsuccessful submission doesn't match the selected source code
			// type
			if (!unsuccessfulSubmissions.equals("")) {
				outputString.append("</br><strong>Unuccessful file submissions:</strong></br>");
				outputString.append("<i>Unsuccessful submissions occur when a file is submitted "
						+ "that does not match the selected source code type.</i></br>");
				outputString.append(unsuccessfulSubmissions);
			}
			unsuccessfulSubmissions = "";

			// Link to general report of submissions
			outputString.append("</br><strong>Reports of file submissions:</strong></br>");
			outputString.append(compareBatchSubmissions());
			request.setAttribute("output", outputString.toString());
			getServletContext().getRequestDispatcher("/Output.jsp").forward(request, response);

			// Current system time to end count of total program run time
			timeEnd = System.currentTimeMillis();

			// Used to display performance counters
			System.out.println("Execution time (ms): " + (timeEnd - timeStart));
			System.out.println("Write time (incl. report generation) (ms): " + (timeWriteTotal));
			System.out.println("Comparison time (ms): " + (timeComparisonTotal));
			System.out.println("Database time (ms): " + (timeDatabaseTotal));
			System.out.println("Comparisions: " + comparisonCount);

			timeWriteTotal = 0;
			comparisonCount = 0;
			timeComparisonTotal = 0;
			timeDatabaseTotal = 0;
		} catch (Exception e) {
			System.err.println("The request could not be forwarded due to an unexpected error: "
					+ e.getMessage());
		}
	}

	/**
	 * Unzips the passed zipped file to the FileUpload folder.
	 * 
	 * @param zippedfile Zipped file to be unzipped
	 * @return True if the file was unzipped successfully
	 */
	private boolean unZipFile(File zippedfile) {
		boolean zipStatus = false;
		// Buffer used by the File Ouput Stream
		byte[] buffer = new byte[1024];

		try {
			// Create a Zip Input Stream to read the zipped file
			ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zippedfile));
			ZipEntry zipEntry = zipInputStream.getNextEntry();

			// Loop through all zip entries in the zipped file
			while (zipEntry != null) {
				String fileNameWithDir = zipEntry.getName();

				String fileName = fileNameWithDir.substring(
						fileNameWithDir.lastIndexOf(File.separatorChar) + 1, fileNameWithDir.length());

				// Ignore files that start with '.' (e.g. .DS_Store) and folders
				// that start with '__' (e.g. __MACOSX)
				if (!fileName.startsWith(".") && !fileNameWithDir.startsWith("__")) {

					// Save the file to the FileUpload folder
					File newFile = new File(getServletContext().getRealPath("") + File.separator
							+ fileUploadPath + File.separator + fileNameWithDir);

					if (!zipEntry.isDirectory()) {
						FileOutputStream fos = new FileOutputStream(newFile);

						// Number of bytes read into buffer
						int length;

						while ((length = zipInputStream.read(buffer)) > 0) {
							fos.write(buffer, 0, length);
						}

						fos.close();
					} else {
						newFile.mkdir();

						// Takes care of the case that the zip file name is
						// different to root folder name
						if (rootZipFolder.equals("")) {
							rootZipFolder = newFile.getCanonicalPath().substring(
									newFile.getCanonicalPath().lastIndexOf(File.separatorChar) + 1,
									newFile.getCanonicalPath().length());
						}
					}
				}
				zipEntry = zipInputStream.getNextEntry();
			}

			zipInputStream.closeEntry();
			zipInputStream.close();

			zipStatus = true;

		} catch (IOException e) {
			System.err.println("Unzip of file: " + zippedfile.getName() + " was unsuccessful.");
		}
		return zipStatus;
	}

	/**
	 * Deletes the directory FileUpload and all its contents. The FileUpload
	 * directory is the temporary location used when extracting a zip archive.
	 */
	private void clearFileUploadDirectory() {
		String rootPath = getServletContext().getRealPath("") + File.separator + fileUploadPath;
		File fileUploadDirectory = new File(rootPath);

		if (fileUploadDirectory.exists()) {
			try {
				deleteFile(fileUploadDirectory);
			} catch (IOException e) {
				System.err.println("An error occured when attempting to clear the FileUpload directory. "
						+ "This may affect further batch submissions.");
			}
		}
	}

	/**
	 * If the passed file is just a file it will delete it, otherwise, it
	 * recursively deletes very file and folder within the passed folder.
	 * 
	 * @param file
	 *            The file or folder to delete
	 * @throws IOException
	 *             An IOException occurred when trying to delete the passed
	 *             file/folder.
	 */
	private void deleteFile(File file) throws IOException {

		if (file.isDirectory()) {
			// Case that the file to delete is a directory
			if (file.list().length == 0) {
				// Case that the directory has files in it
				file.delete();
			} else {
				// Get list of files and other directories in directory
				String fileList[] = file.list();

				for (String child : fileList) {
					// Delete the file/folder, fileName, in the parent
					// directory, file
					File childFile = new File(file, child);
					deleteFile(childFile);
				}

				// If the directory is now empty then delete it
				if (file.list().length == 0) {
					file.delete();
				}
			}

		} else {
			// Case that the file to delete is just a file
			file.delete();
		}
	}
}