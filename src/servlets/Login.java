package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This class handles user logins. The current implementation uses a global user
 * name and password combination.
 * 
 * @author Darryl Meyer
 * @since 2 September 2014
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// Global user name
	private static final String username = "admin";
	// Global password
	private static final String password = "password";
	// Session used to store the login variable, which is used to check whether
	// a user has logged in
	public HttpSession session = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
	}

	/**
	 * Checks to see if the input parameters of the login form match the global
	 * user name and password. If they do match the user has logged in.
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {

		// If the input user name and password match the global user name and
		// password the user has logged in.
		if (request.getParameter("user").equals(username)
				&& request.getParameter("password").equals(password)) {
			session = request.getSession();
			session.setAttribute("login", "true");
			response.sendRedirect("Upload.jsp");
		} else {
			// If the combination is incorrect, the user is prompted to try
			// again
			String responseString = "Login unsuccessful. Username and password combination is incorrect.</br>"
					+ "Please try again. <div class=\"red-letters\"><a href=\"Login.jsp\">Login</a></div>";
			request.setAttribute("output", responseString);
			getServletContext().getRequestDispatcher("/Output.jsp").forward(request, response);
		}
	}

}
