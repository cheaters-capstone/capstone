/**
 * Used for marking a token in similarity checks.
 * 
 * @author Roscoe Jampies
 * @author Craig Feldman
 * 
 * Aug 2014
 */
package domain;

public class TokenItem<T> {
	T content;
	
	boolean marked;

	public TokenItem(T value) {
		content = value;
		marked = false;
	}

	public T getContent() {
		return content;
	}
	
	public void mark(){	
		marked=true;
	}

	public boolean marked() {
		return marked;
	}
}