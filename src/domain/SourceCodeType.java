package domain;

/**
 * Enum class for source code type.
 * 
 * @author Darryl Meyer
 * @since 17 August 2014
 */
public enum SourceCodeType {
	JAVA, PYTHON
}
