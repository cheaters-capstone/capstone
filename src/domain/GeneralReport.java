package domain;

import java.util.ArrayList;
import java.util.Collections;

/**
 * The general report lists all the pair reports that have been generated during
 * a batch comparison. List of pair reports are displayed in a table which
 * includes student numbers, similarity percentages and a link to the detailed
 * pair report.
 * 
 * @since 16 August 2014
 * @author Darryl Meyer
 *
 */
public class GeneralReport extends Report {

	// List containing pair reports to be listed in the general report
	private ArrayList<PairReport> reports;

	/**
	 * The general report takes an array list of all the pair reports generated
	 * during a batch comparison and generates an HTML file listing them,
	 * allowing easy navigation between the individual pair reports.
	 * 
	 * @see PairReport
	 * @param reports
	 *            Array List of all Pair Reports generated during a batch
	 *            comparison.
	 */
	public GeneralReport(ArrayList<PairReport> reports, String date) {
		super(date);

		this.reports = reports;
		generateReport();
	}

	/*
	 * Generate a HTML report containing a listing of all the reports and their
	 * similarities.
	 */
	private void generateReport() {
		// Use string builder to create the html file
		StringBuilder builder = new StringBuilder();

		if (!reports.isEmpty()) {
			// Used to number each row
			int count = 1;

			// Open HTML head, include page title
			builder.append("<head>\n<title>CHEATERS Results</title>");
			// Insert style tag
			builder.append("<style type=\"text/css\">" + getStyleTag() + "</style>");
			builder.append("\n</head>\n<html>");
			// Open HTML body tag
			builder.append("<body>\n<h1>Plagiarism Results</h1>\n");
			// Add date of report generation
			builder.append("<h2>Date: " + dateTimeFormat.format(calendar.getTime()) + "</h2>\n");
			// Explanation of table contents
			builder.append("<p>Similarity 1 is the results of a comparison of Student 1's submission against "
					+ "Student 2's submission and vice versa for Similarity 2.</br>"
					+ "Tokens Matched 1 is the number of tokens matched in Student 1's submission against Student 2's "
					+ "submission out of the total</br>number of tokens in Student 1's submission and vice versa for Tokens Matched 2.</br>"
					+ "To view a detailed report of the comparison click the link in the report column.</p>");

			// The header for the table of pair reports
			builder.append("<div class=\"results_table\"><table><thead><tr><th>Number</th><th>Student 1</th><th>Similarity 1</th>"
					+ "<th>Tokens Matched 1</th><th>Student 2</th><th>Similarity 2</th><th>Tokens Matched 2</th>"
					+ "<th>Report</th><th>Highest Similarity</th></tr></thead><tbody>");

			// Compare the pair reports based on the max similarity between them
			Collections.sort(reports, new PairReportComparator());
			// Sort descending
			Collections.reverse(reports);

			boolean alt = false;
			// For each pair report add a row to the table
			for (PairReport report : reports) {

				// Alternating row have different class styles
				if (alt) {
					builder.append("<tr class=\"alt\"><td>");
					alt = false;
				} else {
					builder.append("<tr><td>");
					alt = true;
				}

				builder.append(count);
				builder.append("</td><td class=\"highlight_blue\">");

				// Student 1
				builder.append(report.getSubmission1().getUser());
				builder.append("</td><td class=\"highlight_blue\">");

				// Similarity 1
				builder.append(decimal.format(report.getPercentage1() * 100) + "%");
				builder.append("</td><td class=\"highlight_blue\">");
				
				// Tokens matched 1
				builder.append(report.getTotalMatchedTokens() + " of " + report.getSubmission1().getTotalTokens());
				builder.append("</td><td class=\"highlight_red\">");

				// Student 2
				builder.append(report.getSubmission2().getUser());
				builder.append("</td><td class=\"highlight_red\">");

				// Similarity 2
				builder.append(decimal.format(report.getPercentage2() * 100) + "%");
				builder.append("</td><td class=\"highlight_red\">");
				
				// Tokens matched 2
				builder.append(report.getTotalMatchedTokens() + " of " + report.getSubmission2().getTotalTokens());
				builder.append("</td><td>");

				// Report
				builder.append("<a href=\"" + report.getFilename().replace("%", "%25") + "\">"
						+ "view detailed report</a></br>");
				count++;
				builder.append("</td><td>");

				// Highest Similarity
				builder.append(decimal.format(Double.parseDouble(report.getMaxSimilarity()) * 100) + "%");
				builder.append("</td></tr>");
			}

			// Close table
			builder.append("</tbody></table></div>");
			// Close body and html tags
			builder.append("</body></html>");

			filename = "report_for_matches_submitted_" + date + ".html";
			html = builder.toString();
		} else {
			// Open HTML head, include page title
			builder.append("<head>\n<title>CHEATERS Results</title>");
			// Insert style tag
			builder.append("<style>" + getStyleTag() + "</style>");
			builder.append("\n</head>\n<html>");

			// Open HTML body tag
			builder.append("<body><h1>Plagiarism Results</h1>");
			builder.append("<p>There was no reports submitted.</p>");
			// Close body and html tags
			builder.append("</body></html>");

			filename = "report_for_matches_submitted_" + date + ".html";
			html = builder.toString();
		}
	}
}
