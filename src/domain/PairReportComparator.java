package domain;

import java.util.Comparator;

/**
 * Used to sort an array list of Pair Reports by comparing the highest
 * similarity between the two reports.
 * 
 * @since 16 August 2014
 * @author Darryl Meyer
 *
 */
public class PairReportComparator implements Comparator<PairReport> {

	@Override
	public int compare(PairReport report1, PairReport report2) {

		return report1.getMaxSimilarity().compareTo(report2.getMaxSimilarity());
	}
}
