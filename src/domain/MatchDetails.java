/**
 * 
 * Stores information about matches that have been found.
 * @author Roscoe Jampies
 * @author Craig Feldman
 * 
 * Aug 2014
 *
 */
package domain;


public class MatchDetails {

	// Position the match begins in the first list
	final int postionInFirst;
	// Position the match begins in the second list
	final int positionInSecond;	
	// length of match
	final int length;
	// used to filter poor matches 
	boolean valid;

	/**
	 * Constructor for a match.
	 * @param pos1 position match begins in first list.
	 * @param pos2 position match begins in second list.
	 * @param length length of match.
	 */
	public MatchDetails(int pos1, int pos2, int length) {
		postionInFirst = pos1;
		positionInSecond = pos2;
		this.length = length;
		valid = true;
	}

	public String toString(){
		return "Match length: " + length + " | Positon(1): " + postionInFirst + " | Position(2): " + positionInSecond;
	}
	
	/** Flag a match for deletion */
	public void invalidate() {
		valid = false; 
	}
	
	//================================================================================
    // Accessors
    //================================================================================
	
	/** @return position the match begins in the first list */
	public int getPositionInFirst() {
		return postionInFirst;
	}

	/** @return position the match begins in the second list */
	public int getPositionInSecond() {
		return positionInSecond;
	}

	/** @return position the length of the match */
	public int getLength() {
		return length;
	}
	
	/** 
	 * Used in {@link #filterMatches()} to remove certain matches. 
	 * @return false if a match has been marked as invalid, true otherwise.
	 */
	public boolean getValid(){
		return valid;
	}
}




