package domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Super class for General and Pair Reports. This class contains information
 * that both classes will require.
 * 
 * @see GeneralReport
 * @see PairReport
 * 
 * @since 16 August 2014
 * @author Darryl Meyer
 * @author Craig Feldman
 *
 */
public class Report {
	// String of the contents of the HTML to be written to file
	protected String html;
	// The name of the file
	protected String filename;
	// The date the report was created
	protected String date;
	// The year the report was created
	protected String year;
	// The base folder of where to save the report
	protected static final String baseReportFolder = "reports";
	// Used when displaying the date and time in the HTML of the report
	protected DateFormat dateTimeFormat;
	// Used to get the year
	protected DateFormat dateYearFormat;
	// Used when saving to storage, saves to folder reports/year/date/
	protected DateFormat dateFormat;
	protected Calendar calendar;
	// Path to location of written report
	protected String absoluteWritePath = baseReportFolder;
	// Path to location of written report
	protected String localWritePath = baseReportFolder;
	// For formatting the percentage to the nearest 2 decimal places
	protected DecimalFormat decimal = new DecimalFormat("#0.00");

	/**
	 * This constructor takes a String of the date and time that the report will
	 * stated it was created.
	 * 
	 * @param date
	 *            String of the date and time the report must be stated to be
	 *            created.
	 */
	public Report(String date) {
		dateTimeFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
		dateYearFormat = new SimpleDateFormat("yyyy");
		dateFormat = new SimpleDateFormat("dd-MM-YYYY_HH-mm");
		calendar = Calendar.getInstance();

		year = dateYearFormat.format(calendar.getTime());
		this.date = date;
	}

	public String toString() {
		return "Filename: " + filename + " | Year: " + year + " | Date: " + date + " | HTML: " + html;
	}

	// ================================================================================
	// Accessors
	// ================================================================================

	public String getDate() {
		return date;
	}

	public String getHtml() {
		return html;
	}

	public String getFilename() {
		return filename;
	}

	public String getYear() {
		return year;
	}

	public String getAbsoluteWritePath() {
		return absoluteWritePath;
	}

	public String getLocalWritePath() {
		return localWritePath;
	}

	// ================================================================================
	// Mutators
	// ================================================================================

	public void setHtml(String html) {
		this.html = html;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setYear(String year) {

		this.year = year;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * The method returns the contents of the style tag for the report. It
	 * contains styles for the results table and font used.
	 * 
	 * @return String of contents of style tag.
	 */
	protected String getStyleTag() {
		String styleTag = "body {font-family: Helvetica, Arial, serif;}"
				+ ".results_table table td.highlight_blue {background-color: rgba(0, 10, 255, 0.1);}"
				+ ".results_table table td.highlight_red {background-color: rgba(255, 0, 0, 0.1);}"
				+ ".results_table table {border-collapse: collapse; text-align: left; width: 100%; } "
				+ ".results_table {border: 1px solid #8C8C8C; border-radius: 3px; } "
				+ ".results_table table td, table th {padding: 3px 10px; } "
				+ ".results_table table thead th {background-color:#8C8C8C; color:#FFFFFF; font-weight: bold; "
				+ "border-left: 1px solid #A3A3A3; } "
				+ ".results_table table thead th:first-child {border: none; } "
				+ ".results_table table tbody td { border-left: 1px solid #DBDBDB; } "
				+ ".results_table table tbody .alt td {background: #EBEBEB; color: #000000; } "
				+ ".results_table table tbody td:first-child {border-left: none; } "
				+ ".results_table table tbody tr:last-child td {border-bottom: none; }";
		return styleTag;
	}

	// ================================================================================
	// File Management
	// ================================================================================

	/**
	 * Writes the report to storage in a folder following the format:
	 * /reports/Year/Date. If the file exists, it will be overwritten. The name
	 * of the report depends on the type and content of the report.
	 */
	public void writeReport(String path) {

		path = path + baseReportFolder + "/" + year + "/" + date;
		localWritePath = baseReportFolder + "/" + year + "/" + date;
		absoluteWritePath = path;

		// System.out.println("Writing report " + filename + " " + path);

		// Try write contents of report to storage
		try {
			File filePath = new File(path);
			File file = new File(path + "/" + filename);

			if (!file.exists()) {

				// Create the path if it does not exist
				filePath.mkdirs();
				// Create the file if it does not exist
				file.createNewFile();
			}

			BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
			bw.write(html);
			bw.close();

		} catch (IOException e) {
			System.out.println("IOException writing report " + path);
			System.out.println(e);
		}
	}

	// ================================================================================
	// Report display
	// ================================================================================
	// used to highlight matching lines based on group
	enum Colours {
		BLUE, FUCHSIA, GREEN, MAROON, NAVY, OLIVE, ORANGE, PURPLE, RED, TEAL;
		public Colours getNextColour() {
			return values()[(ordinal() + 1) % values().length];
		}
	};

}
