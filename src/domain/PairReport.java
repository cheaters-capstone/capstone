package domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Report detailing the comparison of two submissions. The source code of each
 * submission is displayed side by side with highlighting of similar parts.
 * 
 * @since 16 August 2014
 * 
 * @author Craig Feldman
 * @author Darryl Meyer
 *
 */
public class PairReport extends Report {

	private Submission submission1;
	private Submission submission2;
	// Percentage similarity of submission1 when compared to submission2
	private double percentage1;
	// Percentage similarity of submission1 when compared to submission2
	private double percentage2;
	
	// Stores the line numbers of lines that contain a match
	Set<Integer> linesMatched1 = new HashSet<Integer>();
	Set<Integer> linesMatched2 = new HashSet<Integer>();
	
	// Total lines in files
	int totalLines1 = 0;
	int totalLines2 = 0;
	int totalMatchedTokens;
	
	// Top frame
	private String headerFileName;
	// Left frame
	private String htmlFileName1;
	// Right frame
	private String htmlFileName2;


	/**
	 * Constructs a pair report to compare two submissions side by side.
	 * @param submission1 First submission object to be compared against submission2.
	 * @param percentage1 Similarity of submission1 to submission2.
	 * @param submission2 Second submission object to be compared against submission1.
	 * @param percentage2 Similarity of submission2 to submission1
	 * @param date date of report (used for storing the report).
	 */
	public PairReport(Submission submission1, double percentage1, Submission submission2, double percentage2, String date) {

		super(date);

		this.submission1 = submission1;
		this.submission2 = submission2;
		this.percentage1 = percentage1;
		this.percentage2 = percentage2;
		
		htmlFileName1 = submission1.getUser().toString() + "_" + submission2.getUser().toString();
		htmlFileName2 = submission2.getUser().toString() + "_" + submission1.getUser().toString() ;
		headerFileName = (htmlFileName1.compareTo(htmlFileName2) > 0)  ? htmlFileName1 + "_" + "header.html" : htmlFileName2 + "_" + "header.html";

		htmlFileName1 += ".html";
		htmlFileName2 += ".html";

		// Create report structure
		generateReport();
	}

	/**
	 * Generate a HTML file that creates the structure of the pair report.
	 * Contains a header frame and two frames below it for side-by-side comparisons.
	 * Requires a call to {@link #writeReport(String)} to be written to disk.
	 */
	private void generateReport() {
		// Use string builder to create the html file
		StringBuilder builder = new StringBuilder();
		
		builder.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\"" + " \"http://www.w3.org/TR/html4/frameset.dtd\">");		
		builder.append("<html><head><title>Pair Report</title></head>");		
				
		// Generate two frames to compare side by side
		builder.append(
				  "<FRAMESET ROWS=\"215,*\">"
				+ "		<FRAME SRC= \"" + headerFileName + "\" NAME=\"0\">"
				+ "		<FRAMESET COLS=\"50%,50%\">"			
				+ "			<FRAME SRC= \"" + htmlFileName1 + "\" NAME=\"leftFrame\">"
				+ "			<FRAME SRC= \"" + htmlFileName2 + "\" NAME=\"rightFrame\">"
				+ "		</FRAMESET>"
				+ "	</FRAMESET>");

			
		filename = "matches_for_" + submission1.getUser() + "_" + submission1.getAssignment() + "_("
				+ decimal.format(percentage1 * 100) + "%)_" + submission2.getUser() + "_"
				+ submission2.getAssignment() + "_(" + decimal.format(percentage2 * 100) + "%)" + ".html";
		
		
		// html is what will be written when the call to write to disk is made
		html = builder.toString();
	}

	/**
	 * Generates the required HTML files for a pair report.
	 * Generates the HTML files for the left and right frames. 
	 * These two frames contain the code from the submissions, with matching lines highlighted.
	 * Finally, it calls {@link #generateHeaderHTMLFile(String, String)} to generate the header HTML.
	 * 
	 * @param matches A list containing the match details.
	 * @param path where file should be created
	 */
	public void generateHTML(ArrayList<MatchDetails> matches, String path) {
		
		// ===========================================
		// Build the HTML file
		// ===========================================
		path += baseReportFolder + "/" + year + "/" + date;
		File file1 = submission1.getFile();
		File file2 = submission2.getFile();
		
		// Build the left frame
		StringBuilder builder = new StringBuilder();
		
		// Build the right frame
		StringBuilder builder2 = new StringBuilder();
		
		builder.append("<!DOCTYPE html>");
		builder.append("<html lang=\"en\">");
		builder.append("<head><title>Pair Report</title><style type=\"text/css\">body {font-family: Helvetica, Arial, serif;}</style></head>");
		builder.append("<body><p><strong>" + submission1.toString() + "</strong></p>");
		builder.append("<p>");
		
		builder2.append("<!DOCTYPE html>");
		builder2.append("<html lang=\"en\">");
		builder2.append("<head><title>Pair Report</title><style type=\"text/css\">body {font-family: Helvetica, Arial, serif;}</style></head>");
		builder2.append("<body><p><strong>" + submission2.toString() + "</strong></p>");
		builder2.append("<p>");	
		
		
		// ===========================================
		// Mark lines for highlighting
		// ===========================================
		List<Integer> tokenLocations1 = new ArrayList<Integer>(submission1.getTokenLocations());
		List<Integer> tokenLocations2 = new ArrayList<Integer>(submission2.getTokenLocations());	
		
		// stores what colour a line should be
		Colours [] lineColours1 = new Colours[tokenLocations1.get(tokenLocations1.size()-1) + 1]; // left frame
		Colours [] lineColours2 = new Colours[tokenLocations2.get(tokenLocations2.size()-1) + 1]; // right frame
		
		Colours colour = Colours.TEAL;
		// stores line number where token occurs
		int line1;
		int line2;
		
		// Calculates which lines of the file are matches and assigns them a colour.
		for (int i = 0; i < matches.size(); ++i) {
			MatchDetails matchDetails = matches.get(i);
			totalMatchedTokens += matchDetails.getLength();
				for (int j = 0; j < matchDetails.getLength(); ++j) {	
					
					// converts token location to line number where token is
					line1 = tokenLocations1.get(matchDetails.getPositionInFirst() + j);
					line2 = tokenLocations2.get(matchDetails.getPositionInSecond() + j);
					
					// Mark line numbers as similar lines 
					lineColours1[line1] = colour;
					lineColours2[line2] = colour;
					
					// add to matched lines if unique
					linesMatched1.add(line1);
					linesMatched2.add(line2);
				}		
				
			//run through the colour palette
			colour = colour.getNextColour();
		}

		
		// ===========================================
		// Prepare for side by side viewing
		// ===========================================
		
		Scanner scan1 = null;
		Scanner scan2 = null;
		
		// Runs through the submission text and highlights any matching lines
		try {
			scan1 = new Scanner(file1);
			String line;			

			int i = 0;
			while (scan1.hasNextLine()) {
				line = ++i +": ";
				line += scan1.nextLine();
				//replace tab by an HTML printable whitespace
				line = line.replace("\t", "&nbsp&nbsp&nbsp&nbsp"); 	
				
				if (i > lineColours1.length - 1 || lineColours1[i] == null)					
					builder.append(line + "<br>"); // add the new line character
				else if (lineColours1[i] != null) 
					builder.append("<font color=\"" + lineColours1[i] + "\">" + line +"</font><br>"  );	// highlight
			}			
			totalLines1 = i;
			
			scan2 = new Scanner(file2);
			i = 0;
			while (scan2.hasNextLine()) {
				line = ++i +": ";
				line += scan2.nextLine();
				line = line.replace("\t", "&nbsp&nbsp&nbsp&nbsp"); //replace tab by an HTML printable whitespace	
				
				if (i > lineColours2.length - 1 || lineColours2[i] == null)					
					builder2.append(line + "<br>");
				else if (lineColours2[i] != null) 
					builder2.append("<font color=\"" + lineColours2[i] + "\">" + line +"</font><br>"  );
			}			
			totalLines2 = i;
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {		
			scan1.close();
			scan2.close();
		}

		builder.append("</p></body></html>");
		builder2.append("</p></body></html>");		

		
		// ===========================================
		// Write the HTML files to disk
		// ===========================================
		BufferedWriter bw = null;
		try {
			File filePath = new File(path);
			File fileHTML1 = new File(path + "/" + htmlFileName1);
			File fileHTML2 = new File(path + "/" + htmlFileName2);

			filePath.mkdirs();
			fileHTML1.createNewFile();
			fileHTML2.createNewFile();

			bw = new BufferedWriter(new FileWriter(fileHTML1.getAbsoluteFile()));
			bw.write(builder.toString());
			bw.close();

			bw = new BufferedWriter(new FileWriter(fileHTML2.getAbsoluteFile()));
			bw.write(builder2.toString());


		} catch (IOException e) {
			System.out.println("Failed to write HTML File for pair report to" + path);
			System.out.println(e);
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Create the header file
		generateHeaderHTMLFile(headerFileName, path);
	}
	

	/**
	 * Creates the HTML header file that is displayed in the top frame of the pair report.
	 * The HTML files gives the user a brief overview of report.
	 * 
	 * @param fileName name of HTML header file.
	 * @param path path to write to (must be same path as the other pair report HTML files).
	 */
	private void generateHeaderHTMLFile(String fileName, String path) {
		
		// ===========================================
		// Build the header file
		// ===========================================		
		StringBuilder builder = new StringBuilder();		
		// Open HTML head, include page title, meta data and style close
		// head and open html tag
		builder.append("<!DOCTYPE html>\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-\">\n"
				+ "<title>CHEATERS Results</title>\n<style type=\"text/css\">body {font-family: Helvetica, Arial, serif;}</style>\n</head>\n");

		// Open HTML body tag
		builder.append("<body>\n<h2>Pair Report</h2>\n");

		builder.append("<p>Plagiarism assessment results for ");
		builder.append("<strong>" + submission1.getUser() + "</strong> " + submission1.getAssignment()
				+ " and <strong>" + submission2.getUser() + "</strong> " + submission2.getAssignment() + "</p>\n");

		// Add date of report generation
		builder.append("<h3>" + dateTimeFormat.format(calendar.getTime()) + "</h3>\n");

		builder.append("<table border=\"1\">\n<tr>\n<td>" + submission1.getUser() + "'s submission "
				+ submission1.getFile().getName() + " is <strong>" + decimal.format(percentage1 * 100)
				+ "%</strong> similar to " + submission2.getUser() + "'s submission "
				+ submission2.getFile().getName() + "</td>\n</tr>\n");
		builder.append("<tr>\n<td>" + submission2.getUser() + "'s submission "
				+ submission2.getFile().getName() + " is <strong>" + decimal.format(percentage2 * 100)
				+ "%</strong> similar to " + submission1.getUser() + "'s submission "
				+ submission1.getFile().getName() + "</td>\n</tr>\n");

		// Close body and html tags
		builder.append("\n</body>\n</html>");		
		
		// ===========================================
		// Write the file to disk
		// ===========================================
		BufferedWriter bw = null;
		// Try write header HTML files to disk
		try {
			File filePath = new File(path);
			File file = new File(path + "/" + fileName);

			if (!file.exists()) {
				filePath.mkdirs();
				file.createNewFile();
			}

			bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
			bw.write(builder.toString());
						
		} catch (IOException e) {
			System.out.println("Failed to write HTML header file for pair report " + path);
			System.out.println(e);
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	//================================================================================
	// Accessors (used to construct the general report)
	//================================================================================
	
	/** @return How many unique lines were a match in submission1 */
	int getTotalLinesMatched1() {
		return linesMatched1.size();
	}
	
	/** @return How many unique lines were a match in submission2 */
	int getTotalLinesMatched2() {
		return linesMatched2.size();
	}
	
	/** @return Total lines in submission1 file */
	int getTotalLines1() {
		return totalLines1;
	}
	
	/** @return Total lines in submission2 file */
	int getTotalLines2() {
		return totalLines2;
	}
	
	/** @return Total number of tokens matched between the two submissions */
	int getTotalMatchedTokens() {
		return totalMatchedTokens;
	}
	/** @return First submission */
	Submission getSubmission1() {
		return submission1;
	}

	/** @return Second submission */
	Submission getSubmission2() {
		return submission2;
	}
	
	/** @return Score of submission1 against submission2 */
	double getPercentage1() {
		return percentage1;
	}

	/** @return Score of submission2 against submission1 */
	double getPercentage2() {
		return percentage2;
	}

	/** @return score1 if score1 > score 2, else score 2. */
	String getMaxSimilarity() {
		double max = Math.max(percentage1, percentage2);
		return max + "";
	}

}
