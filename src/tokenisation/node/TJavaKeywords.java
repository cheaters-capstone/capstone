/* This file was generated by SableCC (http://www.sablecc.org/). */

package tokenisation.node;

import tokenisation.analysis.*;

@SuppressWarnings("nls")
public final class TJavaKeywords extends Token
{
    public TJavaKeywords(String text)
    {
        setText(text);
    }

    public TJavaKeywords(String text, int line, int pos)
    {
        setText(text);
        setLine(line);
        setPos(pos);
    }

    @Override
    public Object clone()
    {
      return new TJavaKeywords(getText(), getLine(), getPos());
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseTJavaKeywords(this);
    }
}
