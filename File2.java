// Test all java keywords that program will handle

package pizza.jplag; 

import java.io.*;

public class Class {
}// end of class

public void test() {
} // end of method

int i; String text;

synchronized(obj) {
} // end of �synchronized� do 

do {
	
} while(condition);

while (condition) {
}// end of �while�

for (i=0; i<n; i++) {
} // end of �for�

switch (expr) {
} // end of �switch�
case 5:, default:
	try {
		catch(IOException e) {
		}	// end of �catch� 
		
if (test) {
}	else { 
} // end of �if�

(test ? 0: 1 
		) //end of conditional
		
break;
continue;
return x; 
goto 
throw(exception);
test = new A() {
}; //end of inner class 

System.out.print(�!!�);
test = new A();
testarr = new int[i];
i=i+5; i++; i+=5; 
public interface if {
} // end of interface 
