<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<!-- Author: Darryl Meyer
	This JSP handles both batch and single file submissions -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<style>
body {
	font-family: Helvetica, Arial, serif;
}

h1 {
	color: red;
}

.input {
	display: block;
	padding: 5px;
	border: 4px solid #D8D8D8;
	border-radius: 4px;
}

.input:hover {
	border: 4px solid #A0A0A0;
}

.nav {
	width: 100%;
	float: right;
	padding: 0;
}

.nav ul {
	list-style-type: inherit;
	margin: 0;
	padding: 0;
}

.nav ul li a {
	float: right;
	display: block;
	text-decoration: none;
	font-weight: bold;
	color: #CC0000;
}

.nav a:hover {
	color: #FF3300;
}
</style>
<title>Cheaters - Upload</title>
</head>
<body>

	<h1>Cheaters Plagiarism Detection System</h1>
	<div class="nav">
		<%
			if (session.getAttribute("login") != null && session.getAttribute("login").equals("true")) {
		%>
		<ul>
			<li><a href="Logout">Log out</a></li>
		</ul>
		<%
			} else {
		%>
		<ul>
			<li><a href="Login.jsp">Log in</a></li>
		</ul>
		<%
			}
		%>
	</div>
	<p class="helvetica">
		<%
			if (session.getAttribute("login") == null || !session.getAttribute("login").equals("true")) {
		%>
		<strong>Log in to access batch upload feature</strong>
		<%
			}
		%>
	</p>

	<h2>Single File Submission</h2>

	<form method="post" action="Upload" enctype="multipart/form-data">
		Select file to upload: <input class="input" type="file"
			name="uploadSingleFile" required /> <br /> Enter your student
		number: <input class="input" type="text" name="user"
			placeholder="student number" required /> <br /> Enter the
		assignment name: <input class="input" type="text" name="assignment"
			placeholder="assignment" required /> <br /> <input
			type="radio" name="sourceType" value="java">Java<br /> <input
			type="radio" name="sourceType" value="python">Python<br /><br /> 
		<div class="input" style="width: 60px;">
			<input type="submit" value="Submit" />
		</div>
	</form>
	<%
		if (session.getAttribute("login") != null && session.getAttribute("login").equals("true")) {
	%>
	<br />
	<hr>
	<h2>Batch Submission</h2>
	Batch upload allows you to upload a zip file containing submissions.
	<br /> The excepted folder structure for a batch submission must match
	the following:
	<br />
	<i>'assignment name'/'student number'/</i>
	<br /> Where
	<i>'assignment name'</i> is the name of the assignment for which the
	files are submitted
	<br /> and should be the root of the zipped folder and
	<i>'student number'</i> is the unique student number
	<br /> for each student.
	<br />
	<br />
	<form method="post" action="Upload" enctype="multipart/form-data">
		Select file to upload: <input class="input" type="file"
			name="uploadBatchFile" required /><br /> <input type="radio"
			name="sourceType" value="java">Java<br /> <input
			type="radio" name="sourceType" value="python">Python<br /><br /> 
		<div class="input" style="width: 60px;">
			<input type="submit" value="Submit" />
		</div>
	</form>
	<%
		}
	%>
</body>
</html>