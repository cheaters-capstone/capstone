<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<style>
body {
	font-family: Helvetica, Arial, serif;
}

h1 {
	color: red;
}

.input {
	display: block;
	padding: 5px;
	border: 4px solid #D8D8D8;
	border-radius: 4px;
}

.input:hover {
	border: 4px solid #A0A0A0;
}

.nav {
	width: 100%;
	float: right;
	padding: 0;
}

.nav a {
	float: right;
	display: block;
	text-decoration: none;
	font-weight: bold;
	color: #CC0000;
}

.nav a:hover {
	color: #FF3300;
}
</style>
<title>Cheaters - Login</title>
</head>
<body>
	<h1>Cheaters Plagiarism Detection System</h1>
	<div class="nav">
		<a href="Upload.jsp">Submit File</a>
	</div>
	<form action="Login" method="post">

		Username: <input class="input" type="text" name="user" required> <br>
		Password: <input class="input" type="password" name="password" required>
		<br>
		<div class="input" style="width: 50px;">
			<input type="submit" value="Login" />
		</div>
	</form>
</body>
</html>